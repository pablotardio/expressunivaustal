var map = L.map('main_map').setView([-17.7791474,-63.1505137],17);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
 {foo: 'bar', attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'})
 .addTo(map);

 L.marker([-17.778677, -63.149157]).addTo(map);

 $.ajax({
     datatype: "json",
     url: "api/bicicletas",
     success: function(result){
         console.log(result);
         result.bicicletas.forEach( function(bici) {
             L.marker(bici.ubicacion,{title: bici.id}).addTo(map)
         });
     }
 })